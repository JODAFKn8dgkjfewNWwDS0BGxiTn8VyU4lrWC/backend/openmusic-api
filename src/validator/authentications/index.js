const {
  PostAuthenticationPayloadSchema,
  PutAuthenticationPayloadSchema,
  DeleteAuthenticationPayloadSchema,
} = require('./schema');
const InvariantError = require('../../exceptions/InvariantError');

const AuthenticationsValidator = {
  validatePostAuthenticationPayload: (payload) => {
    const validateResult = PostAuthenticationPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
  validatePutAuthenticationPayload: (payload) => {
    const validateResult = PutAuthenticationPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
  validateDeleteAuthenticationPayload: (payload) => {
    const validateResult = DeleteAuthenticationPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
};

module.exports = AuthenticationsValidator;
