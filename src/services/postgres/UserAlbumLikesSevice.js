const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');
const ClientError = require('../../exceptions/ClientError');

class UserAlbumLikesService {
  #pool;
  #cacheService;

  constructor(cacheService) {
    this.#pool = new Pool();
    this.#cacheService = cacheService;
  }

  async likeAlbumById(userId, albumId) {
    const id = nanoid(16);
    const query = {
      text: 'insert into user_album_likes values ($1, $2, $3) returning id',
      values: [`like-${id}`, userId, albumId],
    };

    const isLiked = await this.getAlbumLikeCount(userId, albumId);

    if (isLiked) {
      throw new ClientError('failed to like album. album already liked');
    }

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new activity');
    }

    await this.#cacheService.delete(`album:${albumId}`);
  }

  async getAlbumLikeCount(userId, albumId) {
    const query = {
      text: `select id from user_album_likes
      where user_id = $1 and album_id = $2`,
      values: [userId, albumId],
    };

    const result = await this.#pool.query(query);

    return result.rowCount;
  }

  async unlikeAlbumById(userId, albumId) {
    const query = {
      text: `delete from user_album_likes
      where user_id = $1 and album_id = $2 returning id`,
      values: [userId, albumId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to unlike album');
    }

    await this.#cacheService.delete(`album:${albumId}`);
  }

  async getAlbumLikesById(albumId) {
    try {
      const result = await this.#cacheService.get(`album:${albumId}`);
      return {
        isCache: true,
        likes: JSON.parse(result),
      };
    } catch (error) {
      const query = {
        text: `select id as likes from user_album_likes
        where album_id = $1`,
        values: [albumId],
      };

      const result = await this.#pool.query(query);

      await this.#cacheService.set(`album:${albumId}`, JSON.stringify(result.rowCount));

      return {
        isCache: false,
        likes: result.rowCount,
      };
    }
  }
}

module.exports = UserAlbumLikesService;
