class AuthenticationsHandler {
  #authenticationsService;
  #usersService;
  #tokenManager;
  #validator;

  constructor({ authenticationsService, usersService, tokenManager, validator }) {
    this.#authenticationsService = authenticationsService;
    this.#usersService = usersService;
    this.#tokenManager = tokenManager;
    this.#validator = validator;
  }

  postAuthenticationHandler = async (request, h) => {
    const payload = this.#validator.validatePostAuthenticationPayload(request.payload);

    const id = await this.#usersService.verifyUserCredentials(payload);

    const accessToken = this.#tokenManager.generateAccessToken({ id });
    const refreshToken = this.#tokenManager.generateRefreshToken({ id });

    await this.#authenticationsService.addRefreshToken(refreshToken);

    const response = h.response({
      status: 'success',
      message: 'authentication successfully added',
      data: {
        accessToken,
        refreshToken,
      },
    });
    response.code(201);
    return response;
  };

  putAuthenticationHandler = async (request) => {
    const payload = this.#validator.validatePutAuthenticationPayload(request.payload);

    await this.#authenticationsService.verifyRefreshToken(payload);
    const { id } = this.#tokenManager.verifyRefreshToken(payload);

    const accessToken = this.#tokenManager.generateAccessToken({ id });

    return {
      status: 'success',
      message: 'access token updated successfully',
      data: { accessToken },
    };
  };

  deleteAuthenticationHandler = async (request) => {
    const payload = this.#validator.validateDeleteAuthenticationPayload(request.payload);

    await this.#authenticationsService.verifyRefreshToken(payload);
    await this.#authenticationsService.deleteRefreshToken(payload);

    return {
      status: 'success',
      message: 'refresh token successfully removed',
    };
  };
}

module.exports = AuthenticationsHandler;
