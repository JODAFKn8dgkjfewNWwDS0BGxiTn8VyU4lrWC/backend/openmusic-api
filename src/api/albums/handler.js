const config = require('../../utils/config');

class AlbumsHandler {
  #albumsService;
  #songsService;
  #storageService;
  #userAlbumLikesService;
  #validator;

  constructor({
    albumsService,
    songsService,
    storageService,
    userAlbumLikesService,
    validator,
  }) {
    this.#albumsService = albumsService;
    this.#songsService = songsService;
    this.#storageService = storageService;
    this.#userAlbumLikesService = userAlbumLikesService;
    this.#validator = validator;
  }

  postAlbumHandler = async (request, h) => {
    const payload = this.#validator.validateAlbumPayload(request.payload);
    const albumId = await this.#albumsService.addAlbum(payload);

    const response = h.response({
      status: 'success',
      message: 'album added successfully',
      data: { albumId },
    });
    response.code(201);
    return response;
  };

  getAlbumByIdHandler = async (request) => {
    const { id } = request.params;

    const album = await this.#albumsService.getAlbumById(id);
    album.songs = await this.#songsService.getSongs({ albumId: id });

    return {
      status: 'success',
      data: { album },
    };
  };

  putAlbumByIdHandler = async (request) => {
    const { id } = request.params;

    const payload = this.#validator.validateAlbumPayload(request.payload);
    await this.#albumsService.editAlbumById(id, payload);

    return {
      status: 'success',
      message: 'album updated successfully',
    };
  };

  deleteAlbumByIdHandler = async (request) => {
    const { id } = request.params;

    await this.#albumsService.deleteAlbumById(id);

    return {
      status: 'success',
      message: 'album deleted successfully',
    };
  };

  postAlbumCoversByIdHandler = async (request, h) => {
    const { id: albumId } = request.params;
    const { cover } = request.payload;

    this.#validator.validateImageHeaders(cover.hapi.headers);

    const filename = await this.#storageService.writeFile(cover, cover.hapi);

    await this.#albumsService.setAlbumCoverById(filename, albumId);

    const response = h.response({
      status: 'success',
      message: 'Sampul berhasil diunggah',
    });
    response.code(201);
    return response;
  };

  postAlbumLikeByIdHandler = async (request, h) => {
    const { id: albumId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#albumsService.getAlbumById(albumId);
    await this.#userAlbumLikesService.likeAlbumById(credentialId, albumId);

    const response = h.response({
      status: 'success',
      message: 'successfully liked album',
    });
    response.code(201);
    return response;
  };

  deleteAlbumLikeByIdHandler = async (request) => {
    const { id: albumId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#albumsService.getAlbumById(albumId);
    await this.#userAlbumLikesService.unlikeAlbumById(credentialId, albumId);

    return {
      status: 'success',
      message: 'successfully unliked album',
    };
  };

  getAlbumLikesByIdHandler = async (request, h) => {
    const { id: albumId } = request.params;

    await this.#albumsService.getAlbumById(albumId);
    const result = await this.#userAlbumLikesService.getAlbumLikesById(albumId);

    const response = h.response({
      status: 'success',
      data: { likes: result.likes },
    });
    response.code(200);

    if (result.isCache) {
      response.header('X-Data-Source', 'cache');
    }

    return response;
  };
}

module.exports = AlbumsHandler;
