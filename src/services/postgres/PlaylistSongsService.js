const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');

class PlaylistSongsService {
  #pool;
  #playlistSongActivitiesService;

  constructor(playlistSongActivitiesService) {
    this.#pool = new Pool();
    this.#playlistSongActivitiesService = playlistSongActivitiesService;
  }

  async addSongToPlaylist(playlistId, songId, userId) {
    const id = nanoid(16);
    const query = {
      text: 'insert into playlist_songs values ($1, $2, $3) returning id',
      values: [`playlist_song-${id}`, playlistId, songId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add song to playlist');
    }

    await this.#playlistSongActivitiesService.addPlaylistActivity(
      playlistId,
      songId,
      userId,
      'add'
    );
  }

  async getSongsByPlaylistId(playlistId) {
    const query = {
      text: `select s.id, s.title, s.performer
             from songs as s
             inner join playlist_songs as ps on ps.song_id = s.id
             where ps.playlist_id = $1`,
      values: [playlistId],
    };

    const result = await this.#pool.query(query);

    return result.rows;
  }

  async deleteSongFromPlaylist(playlistId, songId, userId) {
    const query = {
      text: 'delete from playlist_songs where playlist_id = $1 and song_id = $2 returning id',
      values: [playlistId, songId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to delete song from playlist');
    }

    await this.#playlistSongActivitiesService.addPlaylistActivity(
      playlistId,
      songId,
      userId,
      'delete'
    );
  }
}

module.exports = PlaylistSongsService;
