const Joi = require('joi');

const AuthenticationsSchema = {
  PostAuthenticationPayloadSchema: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  }),
  PutAuthenticationPayloadSchema: Joi.object({
    refreshToken: Joi.string().required(),
  }),
  DeleteAuthenticationPayloadSchema: Joi.object({
    refreshToken: Joi.string().required(),
  }),
};

module.exports = AuthenticationsSchema;
