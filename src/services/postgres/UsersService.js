const { Pool } = require('pg');
const { nanoid } = require('nanoid');
const bcrypt = require('bcrypt');
const InvariantError = require('../../exceptions/InvariantError');
const AuthenticationError = require('../../exceptions/AuthenticationError');
const NotFoundError = require('../../exceptions/NotFoundError');

class UsersService {
  #pool;

  constructor() {
    this.#pool = new Pool();
  }

  async addUser({ username, password, fullname }) {
    await this.verifyUsername(username);

    const id = nanoid(16);
    const hashedPassword = await bcrypt.hash(password, 10);
    const query = {
      text: 'insert into users values ($1, $2, $3, $4) returning id',
      values: [`user-${id}`, username, hashedPassword, fullname],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new user');
    }

    return result.rows[0].id;
  }

  async verifyUsername(username) {
    const query = {
      text: 'select username from users where username = $1',
      values: [username],
    };

    const result = await this.#pool.query(query);

    if (result.rowCount) {
      throw new InvariantError('failed to add user. username already in use');
    }
  }

  async verifyUserCredentials({ username, password }) {
    const query = {
      text: 'select id, password from users where username = $1',
      values: [username],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new AuthenticationError('incorrect credentials provided');
    }

    const { id, password: hashedPassword } = result.rows[0];

    const match = await bcrypt.compare(password, hashedPassword);

    if (!match) {
      throw new AuthenticationError('incorrect credentials provided');
    }

    return id;
  }

  async getUserById(id) {
    const query = {
      text: 'select id from users where id = $1',
      values: [id],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('user not found');
    }
  }
}

module.exports = UsersService;
