const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');

class PlaylistSongActivitiesService {
  #pool;

  constructor() {
    this.#pool = new Pool();
  }

  async addPlaylistActivity(playlistId, songId, userId, action) {
    const id = nanoid(16);
    const query = {
      text: 'insert into playlist_song_activities values ($1, $2, $3, $4, $5) returning id',
      values: [`activity-${id}`, playlistId, songId, userId, action],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new activity');
    }

    return result.rows[0].id;
  }

  async getPlaylistActivities(playlistId) {
    const query = {
      text: `select u.username, s.title, pa.action, pa.time
             from playlist_song_activities as pa
             inner join users as u on u.id = pa.user_id
             inner join songs as s on s.id = pa.song_id
             where playlist_id = $1`,
      values: [playlistId],
    };

    const result = await this.#pool.query(query);

    return result.rows;
  }
}

module.exports = PlaylistSongActivitiesService;
