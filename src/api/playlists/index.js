const PlaylistsHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'playlists',
  version: '1.0.0',
  async register(server, options) {
    const playlistsHandler = new PlaylistsHandler(options);
    server.route(routes(playlistsHandler));
  },
};
