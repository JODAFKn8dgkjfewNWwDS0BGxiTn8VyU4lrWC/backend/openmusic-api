class PlaylistsHandler {
  #playlistsService;
  #songsService;
  #playlistSongsService;
  #playlistSongActivitiesService;
  #validator;

  constructor({
    playlistsService,
    songsService,
    playlistSongsService,
    playlistSongActivitiesService,
    validator,
  }) {
    this.#playlistsService = playlistsService;
    this.#songsService = songsService;
    this.#playlistSongsService = playlistSongsService;
    this.#playlistSongActivitiesService = playlistSongActivitiesService;
    this.#validator = validator;
  }

  postPlaylistHandler = async (request, h) => {
    const { id: credentialId } = request.auth.credentials;

    const payload = this.#validator.validatePostPlaylistPayload(request.payload);

    const playlistId = await this.#playlistsService.addPlaylist(payload, credentialId);

    const response = h.response({
      status: 'success',
      message: 'playlist added successfully',
      data: { playlistId },
    });
    response.code(201);
    return response;
  };

  getPlaylistsHandler = async (request) => {
    const { id: ownerId } = request.auth.credentials;

    const playlists = await this.#playlistsService.getPlaylists(ownerId);

    return {
      status: 'success',
      data: { playlists },
    };
  };

  deletePlaylistByIdHandler = async (request) => {
    const { id: playlistId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#playlistsService.verifyPlaylistOwner(playlistId, credentialId);
    await this.#playlistsService.deletePlaylistById(playlistId);

    return {
      status: 'success',
      message: 'playlist deleted successfully',
    };
  };

  postPlaylistSongByIdHandler = async (request, h) => {
    const { id: playlistId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#playlistsService.verifyPlaylistAccess(playlistId, credentialId);
    const { songId } = this.#validator.validatePostSongsPlaylistPayload(request.payload);

    await this.#songsService.getSongById(songId);
    await this.#playlistSongsService.addSongToPlaylist(playlistId, songId, credentialId);

    const response = h.response({
      status: 'success',
      message: 'successfully added song to playlist',
    });
    response.code(201);
    return response;
  };

  getPlaylistSongsByIdHandler = async (request) => {
    const { id: playlistId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#playlistsService.verifyPlaylistAccess(playlistId, credentialId);
    const playlist = await this.#playlistsService.getPlaylistById(playlistId);
    playlist.songs = await this.#playlistSongsService.getSongsByPlaylistId(playlistId);

    return {
      status: 'success',
      data: { playlist },
    };
  };

  deletePlaylistSongByIdHandler = async (request) => {
    const { id: playlistId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#playlistsService.verifyPlaylistAccess(playlistId, credentialId);
    const { songId } = this.#validator.validateDeleteSongsPlaylistPayload(
      request.payload
    );

    await this.#playlistSongsService.deleteSongFromPlaylist(
      playlistId,
      songId,
      credentialId
    );

    return {
      status: 'success',
      message: 'successfully delete song from playlist',
    };
  };

  getPlaylistActivitiesByIdHandler = async (request) => {
    const { id: playlistId } = request.params;
    const { id: credentialId } = request.auth.credentials;

    await this.#playlistsService.verifyPlaylistAccess(playlistId, credentialId);
    const activities =
      await this.#playlistSongActivitiesService.getPlaylistActivities(playlistId);

    return {
      status: 'success',
      data: {
        playlistId,
        activities,
      },
    };
  };
}

module.exports = PlaylistsHandler;
