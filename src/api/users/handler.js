class UsersHandler {
  #service;
  #validator;

  constructor({ service, validator }) {
    this.#service = service;
    this.#validator = validator;
  }

  postUserHandler = async (request, h) => {
    const payload = this.#validator.validateUserPayload(request.payload);

    const userId = await this.#service.addUser(payload);

    const response = h.response({
      status: 'success',
      data: { userId },
    });
    response.code(201);
    return response;
  };
}

module.exports = UsersHandler;
