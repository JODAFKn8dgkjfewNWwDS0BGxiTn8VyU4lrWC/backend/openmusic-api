const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');

class AuthenticationsService {
  #pool;

  constructor() {
    this.#pool = new Pool();
  }

  async addRefreshToken(refreshToken) {
    const query = {
      text: 'insert into authentications values ($1)',
      values: [refreshToken],
    };

    await this.#pool.query(query);
  }

  async verifyRefreshToken({ refreshToken }) {
    const query = {
      text: 'select refresh_token from authentications where refresh_token = $1',
      values: [refreshToken],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('invalid refresh token');
    }
  }

  async deleteRefreshToken({ refreshToken }) {
    const query = {
      text: 'delete from authentications where refresh_token = $1',
      values: [refreshToken],
    };

    await this.#pool.query(query);
  }
}

module.exports = AuthenticationsService;
