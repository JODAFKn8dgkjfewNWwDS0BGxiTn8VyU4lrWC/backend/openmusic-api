class ExportsHandler {
  #producerService;
  #playlistsService;
  #validator;

  constructor({ producerService, playlistsService, validator }) {
    this.#producerService = producerService;
    this.#playlistsService = playlistsService;
    this.#validator = validator;
  }

  postExportPlaylistHandler = async (request, h) => {
    const { id: playlistId } = request.params;
    const { id: ownerId } = request.auth.credentials;
    const { targetEmail } = this.#validator.validateExportPlaylistPayload(
      request.payload
    );

    await this.#playlistsService.verifyPlaylistOwner(playlistId, ownerId);
    await this.#producerService.sendMessage(
      'export:playlists',
      JSON.stringify({ playlistId, targetEmail })
    );

    const response = h.response({
      status: 'success',
      message: 'Permintaan Anda sedang kami proses',
    });
    response.code(201);
    return response;
  };
}

module.exports = ExportsHandler;
