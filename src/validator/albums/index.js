const { AlbumPayloadSchema, ImageHeadersSchema } = require('./schema');
const InvariantError = require('../../exceptions/InvariantError');

const AlbumsValidator = {
  validateAlbumPayload(payload) {
    const validateResult = AlbumPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
  validateImageHeaders(headers) {
    const validateResult = ImageHeadersSchema.validate(headers);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }
  },
};

module.exports = AlbumsValidator;
