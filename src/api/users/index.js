const UsersHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'users',
  version: '1.0.0',
  async register(server, options) {
    const usersHandler = new UsersHandler(options);
    server.route(routes(usersHandler));
  },
};
