class CollaborationsHandler {
  #collaborationsService;
  #playlistsService;
  #usersService;
  #validator;

  constructor({ collaborationsService, playlistsService, usersService, validator }) {
    this.#collaborationsService = collaborationsService;
    this.#usersService = usersService;
    this.#playlistsService = playlistsService;
    this.#validator = validator;
  }

  postCollaborationHandler = async (request, h) => {
    const { id: ownerId } = request.auth.credentials;

    const payload = this.#validator.validateCollaborationPayload(request.payload);

    await this.#usersService.getUserById(payload.userId);
    await this.#playlistsService.verifyPlaylistOwner(payload.playlistId, ownerId);
    const collaborationId = await this.#collaborationsService.addCollaboration(payload);

    const response = h.response({
      status: 'success',
      message: 'collaboration successfully added',
      data: { collaborationId },
    });
    response.code(201);
    return response;
  };

  deleteCollaborationHandler = async (request) => {
    const { id: ownerId } = request.auth.credentials;

    const payload = this.#validator.validateCollaborationPayload(request.payload);

    await this.#playlistsService.verifyPlaylistOwner(payload.playlistId, ownerId);
    await this.#collaborationsService.deleteCollaboration(payload);

    return {
      status: 'success',
      message: 'collaboration successfully deleted',
    };
  };
}

module.exports = CollaborationsHandler;
