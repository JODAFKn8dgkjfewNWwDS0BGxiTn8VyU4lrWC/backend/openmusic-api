const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');
const NotFoundError = require('../../exceptions/NotFoundError');
const { mapSongDBToModel } = require('../../utils');

class SongsService {
  #pool;

  constructor() {
    this.#pool = new Pool();
  }

  async addSong({ title, year, genre, performer, duration, albumId }) {
    const id = nanoid(16);
    const query = {
      text: 'insert into songs values ($1, $2, $3, $4, $5, $6, $7) returning id',
      values: [`song-${id}`, title, year, genre, performer, duration, albumId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new song');
    }

    return result.rows[0].id;
  }

  async getSongs({ title, performer, albumId }) {
    const query = {
      text: 'select id, title, performer from songs',
      values: [],
    };

    if (title || performer) {
      const operator = title && performer ? 'and' : 'or';
      query.text += ` where title ilike $1 ${operator} performer ilike $2`;
      query.values.push(`%${title}%`, `%${performer}%`);
    } else if (albumId) {
      query.text += ' where album_id = $1';
      query.values.push(albumId);
    }

    const result = await this.#pool.query(query);

    return result.rows;
  }

  async getSongById(id) {
    const query = {
      text: 'select * from songs where id = $1',
      values: [id],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('song not found');
    }

    return result.rows.map(mapSongDBToModel)[0];
  }

  async editSongById(id, { title, year, genre, performer, duration, albumId }) {
    const query = {
      text: 'update songs set title = $1, year = $2, genre = $3, performer = $4, duration = $5, album_id = $6  where id = $7 returning id',
      values: [title, year, genre, performer, duration, albumId, id],
    };
    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('failed to update song. song not found');
    }
  }

  async deleteSongById(id) {
    const query = {
      text: 'delete from songs where id = $1 returning id',
      values: [id],
    };
    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('failed to delete song. song not found');
    }
  }
}

module.exports = SongsService;
