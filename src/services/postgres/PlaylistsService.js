const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');
const NotFoundError = require('../../exceptions/NotFoundError');
const AuthorizationError = require('../../exceptions/AuthorizationError');

class PlaylistsService {
  #pool;
  #collaborationsService;

  constructor(collaborationsService) {
    this.#pool = new Pool();
    this.#collaborationsService = collaborationsService;
  }

  async addPlaylist({ name }, ownerId) {
    const id = nanoid(16);
    const query = {
      text: 'insert into playlists values ($1, $2, $3) returning id',
      values: [`playlist-${id}`, name, ownerId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new playlist');
    }

    return result.rows[0].id;
  }

  async getPlaylists(userId) {
    const query = {
      text: `select p.id, p.name, u.username from playlists as p
             inner join users as u on u.id = p.owner
             left join collaborations as c on c.playlist_id = p.id
             where p.owner = $1 or c.user_id = $1`,
      values: [userId],
    };

    const result = await this.#pool.query(query);

    return result.rows;
  }

  async deletePlaylistById(id) {
    const query = {
      text: 'delete from playlists where id = $1 returning id',
      values: [id],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('failed to delete playlist. playlist not found');
    }
  }

  async getPlaylistById(id) {
    const query = {
      text: `select p.id, p.name, u.username
             from playlists as p
             inner join users as u on u.id = p.owner
             where p.id = $1`,
      values: [id],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('playlist not found');
    }

    return result.rows[0];
  }

  async verifyPlaylistOwner(id, owner) {
    const query = {
      text: 'select owner from playlists where id = $1',
      values: [id],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('playlist not found');
    }

    const playlist = result.rows[0];

    if (playlist.owner !== owner) {
      throw new AuthorizationError("you don't have access to this resource");
    }
  }

  async verifyPlaylistAccess(id, userId) {
    try {
      await this.verifyPlaylistOwner(id, userId);
    } catch (error) {
      if (error instanceof NotFoundError) {
        throw error;
      }

      try {
        await this.#collaborationsService.verifyCollaborator(id, userId);
      } catch {
        throw error;
      }
    }
  }
}

module.exports = PlaylistsService;
