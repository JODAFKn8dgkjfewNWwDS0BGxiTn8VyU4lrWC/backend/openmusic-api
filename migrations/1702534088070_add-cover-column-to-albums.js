/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.addColumns('albums', {
    cover: 'text',
  });
};

exports.down = (pgm) => {
  pgm.dropColumns('albums', ['cover'], {
    cascade: true,
  });
};
