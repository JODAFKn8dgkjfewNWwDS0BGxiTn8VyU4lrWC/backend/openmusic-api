const { UserPayloadSchema } = require('./schema');
const InvariantError = require('../../exceptions/InvariantError');

const SongsValidator = {
  validateUserPayload(payload) {
    const validateResult = UserPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
};

module.exports = SongsValidator;
