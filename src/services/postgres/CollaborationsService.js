const { Pool } = require('pg');
const { nanoid } = require('nanoid');
const InvariantError = require('../../exceptions/InvariantError');

class CollaborationsService {
  #pool;

  constructor() {
    this.#pool = new Pool();
  }

  async addCollaboration({ playlistId, userId }) {
    const id = nanoid(16);
    const query = {
      text: 'insert into collaborations values ($1, $2, $3) returning id',
      values: [`collab-${id}`, playlistId, userId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new collaboration');
    }

    return result.rows[0].id;
  }

  async deleteCollaboration({ playlistId, userId }) {
    const query = {
      text: 'delete from collaborations where playlist_id = $1 and user_id = $2 returning id',
      values: [playlistId, userId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to delete collaboration');
    }
  }

  async verifyCollaborator(playlistId, userId) {
    const query = {
      text: 'select * from collaborations where playlist_id = $1 and user_id = $2',
      values: [playlistId, userId],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('collaboration failed to verify');
    }
  }
}

module.exports = CollaborationsService;
