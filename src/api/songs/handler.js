class SongsHandler {
  #service;
  #validator;

  constructor({ service, validator }) {
    this.#service = service;
    this.#validator = validator;
  }

  postSongHandler = async (request, h) => {
    const payload = this.#validator.validateSongPayload(request.payload);
    const songId = await this.#service.addSong(payload);

    const response = h.response({
      status: 'success',
      message: 'song added successfully',
      data: { songId },
    });
    response.code(201);
    return response;
  };

  getSongsHandler = async (request) => {
    const songs = await this.#service.getSongs(request.query);

    return {
      status: 'success',
      data: { songs },
    };
  };

  getSongByIdHandler = async (request) => {
    const { id } = request.params;

    const song = await this.#service.getSongById(id);

    return {
      status: 'success',
      data: { song },
    };
  };

  putSongByIdHandler = async (request) => {
    const { id } = request.params;

    const payload = this.#validator.validateSongPayload(request.payload);
    await this.#service.editSongById(id, payload);

    return {
      status: 'success',
      message: 'song updated successfully',
    };
  };

  deleteSongByIdHandler = async (request) => {
    const { id } = request.params;

    await this.#service.deleteSongById(id);

    return {
      status: 'success',
      message: 'song deleted successfully',
    };
  };
}

module.exports = SongsHandler;
