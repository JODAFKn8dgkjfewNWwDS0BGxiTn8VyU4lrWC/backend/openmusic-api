const {
  PostPlaylistPayloadSchema,
  PostSongsPlaylistPayloadSchema,
  DeleteSongsPlaylistPayloadSchema,
} = require('./schema');
const InvariantError = require('../../exceptions/InvariantError');

const PlaylistsValidator = {
  validatePostPlaylistPayload: (payload) => {
    const validateResult = PostPlaylistPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
  validatePostSongsPlaylistPayload: (payload) => {
    const validateResult = PostSongsPlaylistPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
  validateDeleteSongsPlaylistPayload: (payload) => {
    const validateResult = DeleteSongsPlaylistPayloadSchema.validate(payload);

    if (validateResult.error) {
      throw new InvariantError(validateResult.error.message);
    }

    return validateResult.value;
  },
};

module.exports = PlaylistsValidator;
