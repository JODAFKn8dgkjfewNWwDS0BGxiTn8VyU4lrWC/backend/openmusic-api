const routes = require('./routes');

module.exports = {
  name: 'uploads',
  version: '1.0.0',
  async register(server) {
    server.route(routes);
  },
};
