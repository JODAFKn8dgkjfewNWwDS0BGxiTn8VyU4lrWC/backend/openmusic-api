const Joi = require('joi');

const PlaylistsSchema = {
  PostPlaylistPayloadSchema: Joi.object({
    name: Joi.string().required(),
  }),
  PostSongsPlaylistPayloadSchema: Joi.object({
    songId: Joi.string().required(),
  }),
  DeleteSongsPlaylistPayloadSchema: Joi.object({
    songId: Joi.string().required(),
  }),
};

module.exports = PlaylistsSchema;
