const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvariantError');
const NotFoundError = require('../../exceptions/NotFoundError');

class AlbumsService {
  #pool;

  constructor() {
    this.#pool = new Pool();
  }

  async addAlbum({ name, year }) {
    const id = nanoid(16);
    const query = {
      text: 'insert into albums values ($1, $2, $3) returning id',
      values: [`album-${id}`, name, year],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('failed to add new album');
    }

    return result.rows[0].id;
  }

  async getAlbumById(id) {
    const query = {
      text: `select id, name, year, cover as "coverUrl"
      from albums where id = $1`,
      values: [id],
    };

    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('album not found');
    }

    return result.rows[0];
  }

  async editAlbumById(id, { name, year }) {
    const query = {
      text: 'update albums set name = $1, year = $2 where id = $3 returning id',
      values: [name, year, id],
    };
    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('failed to update album. album not found');
    }
  }

  async deleteAlbumById(id) {
    const query = {
      text: 'delete from albums where id = $1 returning id',
      values: [id],
    };
    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('failed to delete album. album not found');
    }
  }

  async setAlbumCoverById(filename, id) {
    const query = {
      text: 'update albums set cover = $1 where id = $2 returning id',
      values: [filename, id],
    };
    const result = await this.#pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('failed to add album cover. album not found');
    }
  }
}

module.exports = AlbumsService;
