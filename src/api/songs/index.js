const SongsHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'songs',
  version: '1.0.0',
  async register(server, options) {
    const songsHandler = new SongsHandler(options);
    server.route(routes(songsHandler));
  },
};
