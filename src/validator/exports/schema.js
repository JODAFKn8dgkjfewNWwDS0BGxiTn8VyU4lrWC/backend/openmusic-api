const Joi = require('joi');

const ExportsSchema = {
  ExportPlaylistPayloadSchema: Joi.object({
    targetEmail: Joi.string().email({ tlds: true }).required(),
  }),
};

module.exports = ExportsSchema;
